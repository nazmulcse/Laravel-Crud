<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/','TasksController@index');
Route::get('/create','TasksController@create');
Route::post('/store','TasksController@store');
Route::get('/show/{id}',['uses'=>'TasksController@show','as'=>'tasks.show']);
Route::get('/crud/{id}/edit',['uses'=>'TasksController@edit','as'=>'tasks.edit']);
Route::put('/crud/{id}',['uses'=>'TasksController@update','as'=>'task.update']);
Route::delete('/crud/{id}',['uses'=>'TasksController@destroy','as'=>'task.destroy']);
Route::get('users/register','Auth\AuthController@getRegister');
Route::post('users/register','Auth\AuthController@postRegister');
Route::get('users/logout', 'Auth\AuthController@getLogout');
Route::get('/home', 'TasksController@index');
