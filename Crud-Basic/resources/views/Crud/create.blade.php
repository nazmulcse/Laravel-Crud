@extends('Layout.master')
@section('content')
    <div class="row col-md-4">
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $errors)
                <p>{{$errors}}</p>
                @endforeach
        </div>
        @endif
    @if(Session::has('message'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em></div>
    @endif
    {!! Form::open([
    'action' => 'TasksController@store'
]) !!}
    <div class="form-group">
        {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email', 'Email address:', ['class' => 'control-label']) !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('password','Password:',['class'=>'control-label']) !!}
        {!! Form::password('password',['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('contact_number', 'Contact Number:', ['class' => 'control-label']) !!}
        {!! Form::text('contact_number', null, ['class' => 'form-control']) !!}
    </div>
    {!! Form::submit('Submite',['class'=>'form-control']) !!}
    {!! Form::close() !!}
</div>
@endsection

{{--<form role="form" action="/store" method="post">--}}
    {{--{!! csrf_field() !!}--}}
    {{--<div class="form-group">--}}
        {{--<label for="name">Name:</label>--}}
        {{--<input type="text" class="form-control" name="name" id="name">--}}
    {{--</div>--}}
    {{--<div class="form-group">--}}
        {{--<label for="email">Email address:</label>--}}
        {{--<input type="email" class="form-control" name="email" id="email">--}}
    {{--</div>--}}
    {{--<div class="form-group">--}}
        {{--<label for="contact_number">Contact Number:</label>--}}
        {{--<input type="text" class="form-control" name="contact_number" id="contact_number">--}}
    {{--</div>--}}
    {{--<button type="submit" class="btn btn-default">Submit</button>--}}
{{--</form>--}}

