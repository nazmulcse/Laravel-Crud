@extends('Layout.master')
@section('content')
    <div class="row col-md-8">
        @if(Session::has('message'))
            <div class="alert alert-success">
                {{Session::get('message')}}
            </div>
        @endif
    <h1>User Lists</h1>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>E-Mail</th>
                    <th>Contact Number</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tasks as $task)
                <tr>
                    <td>{{$task->name}}</td>
                    <td>{{$task->email}}</td>
                    <td>{{$task->contact_number}}</td>
                    <td>
                        <a href="{{route('tasks.show',$task->id)}}" class="btn btn-info">View</a>
                        <a href="{{route('tasks.edit',$task->id)}}" class="btn btn-info">Edit</a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'route'=>['task.destroy',$task->id]
                        ]) !!}
                        {!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
@endsection