@extends('Layout.master')
@section('content')
    <div class="row col-md-4">
        @include('Crud.partials.alerts.errors')
        <h1>Update Information</h1>
        <hr>
{!!Form::model($tasks,['method'=>'put','route'=>['task.update',$tasks->id]])
 !!}
<div class="form-group">
    {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('email', 'Email address:', ['class' => 'control-label']) !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('contact_number', 'Contact Number:', ['class' => 'control-label']) !!}
    {!! Form::text('contact_number', null, ['class' => 'form-control']) !!}
</div>
{!! Form::submit('Update',['class'=>'form-control']) !!}
{!! Form::close() !!}
    </div>
@endsection